﻿using System;
using System.Collections.Generic;
using System.IO;
using Nancy;
using Newtonsoft.Json;

namespace DotNetCore.WebApi.Docker.Demo.Controller
{
    public class HomeController : NancyModule
    {
        public HomeController()
        {
            this.RegisterRoutes();
        }

        public object GetTest()
        {
            return Response.AsJson(new { Message = "This is a simple test" });
        }

        public object PostTest(Stream request)
        {
            var body = this.Request.Body;
            int length = (int)body.Length; // this is a dynamic variable
            byte[] data = new byte[length];
            body.Read(data, 0, length);
            var test = System.Text.Encoding.Default.GetString(data);
            var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(test);
            Console.WriteLine(test);
            return HttpStatusCode.OK;
        }

        private void RegisterRoutes()
        {
            this.Get("test", args => this.GetTest());
            this.Post("test",args => this.PostTest(this.Request.Body));
        }
    }
}
